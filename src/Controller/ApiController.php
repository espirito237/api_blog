<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="app_api")
     */
    public function index(): JsonResponse
    {
        return new JsonResponse("Bienvenue sur votre Api rest");
    }


     /**
     * @Route("/doc", name="app_doc")
     */
    public function doc(): JsonResponse
    {
        return new JsonResponse([

            "titre" => "Api rest",
            "sous-titre" => [

                "login" => "rest login",
                "message" => "rest message"
            ]

        ]);
    }

}
