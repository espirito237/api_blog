<?php

namespace App\Controller\api;

use DateTime;
use App\Entity\TUser;
use App\Shared\Globals;
use App\Shared\ErrorHttp;
use App\Repository\TPaysRepository;
use App\Repository\TUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

/**
 * @Route("/api")
 */
class SecurityController extends AbstractController
{
    private Globals $globals;

    public function __construct(Globals $globals)
    {
        $this->globals = $globals;
    }

    /**
     * @Route("/login", name="app_login", methods={"POST", "HEAD"})
     */
    public function login(
        TUserRepository $userRepo, 
        JWTTokenManagerInterface $token, 
        UserPasswordHasherInterface $passwordHasher): JsonResponse
    {
        $data = $this->globals->jsondecode();
        
        if (!isset(
            $data->username,
            $data->password
        )) {
                return new JsonResponse("error", 500);
        } else {

            $user = $userRepo->findOneBy(['Username' => $data->username]);

            if(!$user) {
                
                return $this->globals->error(ErrorHttp::USER_NOT_FOUND);
            } 

            if(!$passwordHasher->isPasswordValid($user, $data->password)) {
                
                return $this->globals->error(ErrorHttp::PASSWORD_INVALID);
            }

            return $this->globals->success([
                'username' => $user->getUsername(),
                'token' => $token->create($user)
            ]);
        }
    }

    /**
     * @Route("/register", name="app_register", methods={"POST", "HEAD"})
     */
    public function register(
        UserPasswordHasherInterface $passwordHasher,
        EntityManagerInterface $entityManager,
        TPaysRepository $PaysRepository): JsonResponse
    {

        $data = $this->globals->jsondecode();

        if (!isset(
            $data->username,
            $data->firstname,
            $data->lastname,
            $data->password,
            $data->fk_pays
        )) {

            return $this->globals->error(ErrorHttp::FORM_INVALID);
        } else {

            $user = new TUser();

            $pays = $PaysRepository->findOneBy(['id' => $data->fk_pays, 'active' => true]);

            if(!$pays) {
                return $this->globals->error(ErrorHttp::PAYS_NOT_FOUND);
            }

            $user->setUsername($data->username)
                ->setFirstname($data->firstname)
                ->setLastname($data->lastname)
                ->setActive(true)
                ->setFkPays($pays)
                ->setRoles(["ROLE_AUTHOR"])
                ->setCreatedAt(new \DateTime());

            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $data->password
            );

            $user->setPassword($hashedPassword);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->globals->success($user->tojson());
        }
    }
}
