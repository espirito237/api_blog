<?php

namespace App\Controller\api\secure;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ArticleController extends AbstractController
{
    /**
     * @Route("/api/secure/article", name="app_article")
     * @IsGranted("ROLE_AUTHOR")
     */
    public function index(): JsonResponse
    {
        return new JsonResponse("article");
    }
}
