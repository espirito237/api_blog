<?php

namespace App\Entity;

use App\Entity\base\TraitEntity;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TCommentRepository;

/**
 * @ORM\Entity(repositoryClass=TCommentRepository::class)
 */
class TComment
{
    use TraitEntity;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=TUser::class, inversedBy="tComments")
     */
    private $fk_user;

    /**
     * @ORM\ManyToOne(targetEntity=TArticle::class, inversedBy="tComments")
     */
    private $fk_article;

    /**
     * @ORM\Column(type="text")
     */
    private $commentaire;

    public function tojson(): array
    {
        return [
            
            'createdAt' => $this->createdAt ? $this->createdAt->format('c') : null,
            'active' => $this->active,
            'id' => $this->id,
            'commentaire' => $this->commentaire,
            'fk_user' => $this->fk_user ? $this->fk_user->tojson() : null,
            'fk_article' => $this->fk_article ? $this->fk_article->tojson() : null,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkUser(): ?TUser
    {
        return $this->fk_user;
    }

    public function setFkUser(?TUser $fk_user): self
    {
        $this->fk_user = $fk_user;

        return $this;
    }

    public function getFkArticle(): ?TArticle
    {
        return $this->fk_article;
    }

    public function setFkArticle(?TArticle $fk_article): self
    {
        $this->fk_article = $fk_article;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }
}
