<?php

namespace App\Entity;

use App\Entity\base\TraitEntity;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TPaysRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=TPaysRepository::class)
 */
class TPays
{
    
    use TraitEntity;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=TUser::class, mappedBy="fk_pays")
     */
    private $tUsers;

    public function __construct()
    {
        $this->tUsers = new ArrayCollection();
    }

    public function tojson(): array
    {
        return [
            
            'createdAt' => $this->createdAt ? $this->createdAt->format('c') : null,
            'active' => $this->active,
            'id' => $this->id,
            'nom' => $this->nom,
            'description' => $this->description,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, TUser>
     */
    public function getTUsers(): Collection
    {
        return $this->tUsers;
    }

    public function addTUser(TUser $tUser): self
    {
        if (!$this->tUsers->contains($tUser)) {
            $this->tUsers[] = $tUser;
            $tUser->setFkPays($this);
        }

        return $this;
    }

    public function removeTUser(TUser $tUser): self
    {
        if ($this->tUsers->removeElement($tUser)) {
            // set the owning side to null (unless already changed)
            if ($tUser->getFkPays() === $this) {
                $tUser->setFkPays(null);
            }
        }

        return $this;
    }
}
