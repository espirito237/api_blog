<?php

namespace App\Entity;

use App\Entity\base\TraitEntity;
use App\Repository\TArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TArticleRepository::class)
 */
class TArticle
{
    
    use TraitEntity;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=TUser::class, inversedBy="tArticles")
     */
    private $fk_user;

    /**
     * @ORM\OneToMany(targetEntity=TComment::class, mappedBy="fk_article")
     */
    private $tComments;

    /**
     * @ORM\ManyToMany(targetEntity=TCategorie::class, inversedBy="tArticles")
     */
    private $fk_categorie;

    public function __construct()
    {
        $this->tComments = new ArrayCollection();
        $this->fk_categorie = new ArrayCollection();
    }


    public function tojson(): array
    {
        return [
            'createdAt' => $this->createdAt ? $this->createdAt->format('c') : null,
            'active' => $this->active,
            'id' => $this->id,
            'titre' => $this->titre,
            'description' => $this->description,
            'fk_user' => $this->fk_user ? $this->fk_user->tojson() : null,
            'fk_categorie' => $this->fk_categories ? $this->fk_categories->tojson() : null,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getFkUser(): ?TUser
    {
        return $this->fk_user;
    }

    public function setFkUser(?TUser $fk_user): self
    {
        $this->fk_user = $fk_user;

        return $this;
    }

    /**
     * @return Collection<int, TComment>
     */
    public function getTComments(): Collection
    {
        return $this->tComments;
    }

    public function addTComment(TComment $tComment): self
    {
        if (!$this->tComments->contains($tComment)) {
            $this->tComments[] = $tComment;
            $tComment->setFkArticle($this);
        }

        return $this;
    }

    public function removeTComment(TComment $tComment): self
    {
        if ($this->tComments->removeElement($tComment)) {
            // set the owning side to null (unless already changed)
            if ($tComment->getFkArticle() === $this) {
                $tComment->setFkArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TCategorie>
     */
    public function getFkCategorie(): Collection
    {
        return $this->fk_categorie;
    }

    public function addFkCategorie(TCategorie $fkCategorie): self
    {
        if (!$this->fk_categorie->contains($fkCategorie)) {
            $this->fk_categorie[] = $fkCategorie;
        }

        return $this;
    }

    public function removeFkCategorie(TCategorie $fkCategorie): self
    {
        $this->fk_categorie->removeElement($fkCategorie);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
